package com.vadenent.project3;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class SessionListener implements HttpSessionListener {
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		
		Integer sessionCounter = 0;
		se.getSession().setAttribute("sessionCounter", sessionCounter);
				
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		
	}	
	
}
