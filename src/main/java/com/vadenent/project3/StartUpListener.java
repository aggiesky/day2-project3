package com.vadenent.project3;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class StartUpListener implements ServletContextListener {
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		Integer appCounter = 0;
		sce.getServletContext().setAttribute("appCounter", appCounter);
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
	}	
	
}
