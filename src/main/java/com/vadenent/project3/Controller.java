package com.vadenent.project3;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = {"", "/main"})
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	
		// appCounter counts each page view for the app - including all users
		// sessionCounter counts each page view for each user - i.e. a separate count is maintained for each user
		
		// retrieving the current values of the counters
		Integer appCounter = (Integer) request.getSession().getServletContext().getAttribute("appCounter");
		Integer sessionCounter = (Integer) request.getSession().getAttribute("sessionCounter");
		
		// saving off the incremented value of the counters
		request.getSession().getServletContext().setAttribute("appCounter", theIncrementer(appCounter));
		request.getSession().setAttribute("sessionCounter", theIncrementer(sessionCounter));
		
		// returning to the view with the updated counter values
		request.getRequestDispatcher("/WEB-INF/MainPage.jsp").forward(request, response);

    }   // end of processRequest

	private Integer theIncrementer(Integer num) {
		num = num + 1;
		return num;
	}

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
}
