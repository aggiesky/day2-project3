<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Counter App</title>
        <!-- Required meta tags -->
    	<meta charset="utf-8">
    	<link href="css/basic.css" rel="stylesheet" type="text/css" media="screen">
    		  
    </head>
    
    <body>
    
    <a href="main">Update</a>
    
    <div class="jumbotron">
      	<h1 class="display-4">App Counter &nbsp ${appCounter}</h1>
      	<p class="lead">This counter shows the total number of page views all users combined.</p>
    </div>

	<div class="jumbotron">
 	 	<h1 class="display-4">Session Counter &nbsp ${sessionCounter}</h1>
  		<p class="lead">This counter shows the total number of page views for just this session.</p>
	</div>
	
	
    </body>
</html>
